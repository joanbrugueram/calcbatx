CalcBatx - Programa de càlcul matemàtic
---------------------------------------
CalcBatx és un programa de càlcul matemàtic que vaig desenvolupar com a Treball de Recerca durant el Batxillerat.

El programa és capaç de realitzar alguns dels càlculs més comuns dels estudiants de Batxillerat, com per exemple realitzar computacions, dibuixar gràfics, trobar arrels de polinomis, derivació (simbòlica), integració (numèrica), etc. També permet que l'usuari defineixi funcions ell mateix.

En el repositori també està inclosa la memòria del projecte i la presentació (podeu consultar-los per veure imatges del projecte).

**Notes**

El programa té alguns *bugs* i limitacions, principalment a causa de l'ús de aritmètica de precisió finita (*double*) i l'ús d'algorismes més aviat rudimentaris (mètode de la bisecció, mètode dels trapezis, etc.).

El codi està bastant ben escrit i documentat, tot i que en alguns punts és millorable.



**Detalls tècnics**

El programa està escrit en *Java* utilitzant la llibreria *Swing* per la interfície gràfica. Llicenciat sota la llicència *GNU GPL3*.